package com.subaharan.subaharantask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PICK_FILE = 1;
    Button buttonClick;
    private File selectedFile;
    TextView textView;
    private RecyclerView recyclerView;
    private AlbumsAdapter adapter;
    int sizeAdapter=0;
    ArrayList<Integer> datacount=new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonClick = (Button) findViewById(R.id.buttonClick);
        textView = (TextView) findViewById(R.id.textView);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        buttonClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FilePicker.class);
                startActivityForResult(intent, REQUEST_PICK_FILE);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_PICK_FILE:

                    if (data.hasExtra(FilePicker.EXTRA_FILE_PATH)) {

                        selectedFile = new File
                                (data.getStringExtra(FilePicker.EXTRA_FILE_PATH));
                        textView.setText(selectedFile.getPath());


                        File file = new File(selectedFile.getPath());
                        BufferedReader bufferedReader = null;

                        String inputLine = null;
                        Map<String, Integer> crunchifyMap = new HashMap<>();

                        try {
                            bufferedReader = new BufferedReader(new FileReader(file));
                            while ((inputLine = bufferedReader.readLine()) != null) {
                                String[] words = inputLine.split("[ \n\t\r.,;:!?(){}]");

                                for (int counter = 0; counter < words.length; counter++) {
                                    String key = words[counter].toLowerCase(); // remove .toLowerCase for Case Sensitive result.
                                    if (key.length() > 0) {
                                        if (crunchifyMap.get(key) == null) {
                                            crunchifyMap.put(key, 1);
                                        } else {
                                            int value = crunchifyMap.get(key).intValue();
                                            value++;
                                            crunchifyMap.put(key, value);

                                        }
                                    }
                                }
                            }

                            sizeAdapter=datacount.size()+1;
                            Set<Map.Entry<String, Integer>> entrySet = crunchifyMap.entrySet();
                            List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(entrySet);
                            Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
                                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                                    return (o1.getValue()).compareTo(o2.getValue());
                                }
                            });

                            for (Map.Entry<String, Integer> entry : list) {
                                System.out.println(entry.getKey() + " ==== " + entry.getValue());
                                if(!datacount.contains(entry.getValue()/10)){
                                    datacount.add(entry.getValue()/10);
                                }
                            }

                            adapter = new AlbumsAdapter(MainActivity.this, list);

                            LinearLayoutManager llm = new LinearLayoutManager(this);
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            recyclerView.setLayoutManager(llm);
//                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapter);


                            System.out.println("Words" + "\t\t" + "# of Occurances");
                            for (Map.Entry<String, Integer> entry : entrySet) {


                                System.out.println(entry.getKey() + "\t\t" + entry.getValue());
                                Log.e("MainActivity", entry.getKey() + "\t\t" + entry.getValue());
                            }
                            List<Integer> myTopOccurrence = crunchifyFindMaxOccurance(crunchifyMap, 1);
                            System.out.println("\nMaixmum Occurance of Word in file: ");

                            for (Integer result : myTopOccurrence) {

                                System.out.println("==> " + result);

                            }


                        } catch (IOException error) {
                            System.out.println("Invalid File");
                        } finally {
//                            bufferedReader.close();
                        }


                    }
                    break;
            }
        }
    }


    /**
     * @param map = All Words in map
     * @param n   = How many top elements you want to print? If n=1 it will print highest occurrence word. If n=2 it
     *            will print top 2 highest occurrence words.
     * @returns list of String
     */
    public List<Integer> crunchifyFindMaxOccurance(Map<String, Integer> map, int n) {
        List<CrunchifyComparable> l = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet())
            l.add(new CrunchifyComparable(entry.getKey(), entry.getValue()));

        Collections.sort(l);
        List<Integer> list = new ArrayList<>();
        for (CrunchifyComparable w : l.subList(0, n))
            list.add( w.numberOfOccurrence);
        return list;
    }

    class CrunchifyComparable implements Comparable<CrunchifyComparable> {
        public String wordFromFile;
        public int numberOfOccurrence;

        public CrunchifyComparable(String wordFromFile, int numberOfOccurrence) {
            super();
            this.wordFromFile = wordFromFile;
            this.numberOfOccurrence = numberOfOccurrence;
        }

        @Override
        public int compareTo(CrunchifyComparable arg0) {
            int crunchifyCompare = Integer.compare(arg0.numberOfOccurrence, this.numberOfOccurrence);
            return crunchifyCompare != 0 ? crunchifyCompare : wordFromFile.compareTo(arg0.wordFromFile);
        }

        @Override
        public int hashCode() {
            final int uniqueNumber = 19;
            int crunchifyResult = 9;
            crunchifyResult = uniqueNumber * crunchifyResult + numberOfOccurrence;
            crunchifyResult = uniqueNumber * crunchifyResult + ((wordFromFile == null) ? 0 : wordFromFile.hashCode());
            return crunchifyResult;
        }

        @Override
        public boolean equals(Object crunchifyObj) {
            if (this == crunchifyObj)
                return true;
            if (crunchifyObj == null)
                return false;
            if (getClass() != crunchifyObj.getClass())
                return false;
            CrunchifyComparable other = (CrunchifyComparable) crunchifyObj;
            if (numberOfOccurrence != other.numberOfOccurrence)
                return false;
            if (wordFromFile == null) {
                if (other.wordFromFile != null)
                    return false;
            } else if (!wordFromFile.equals(other.wordFromFile))
                return false;
            return true;
        }
    }


    public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.MyViewHolder> {

        private Context mContext;
        private List<Map.Entry<String, Integer>> albumList;
        CardView ll;


        public class MyViewHolder extends RecyclerView.ViewHolder {
            //            public TextView title;
            public LinearLayout ll1;
            public CardView cardView;

            public MyViewHolder(View view) {
                super(view);
//                title = (TextView) view.findViewById(R.id.title);
                ll1 = (LinearLayout) view.findViewById(R.id.ll);

            }
        }


        public AlbumsAdapter(Context mContext, List<Map.Entry<String, Integer>> crunchifyMap) {
            this.mContext = mContext;
            this.albumList = crunchifyMap;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
//            Map<String, Integer> album = albumList;
           /* View v = new View(getApplicationContext());
            v.setMinimumHeight(2);
            v.setBackgroundColor(Color.BLUE);
            holder.ll1.addView(v);*/
int pos=datacount.get(position);
            for(int i=0;i<datacount.size();i++){
                Log.e("MainActivity"," data : "+datacount.get(i));
            }
            TextView title = new TextView(getApplicationContext());
            int start, end;
            start = (pos * 10) + 1;
            end = (pos + 1) * 10;
            title.setText(start + " - " + end);
            title.setTextColor(Color.BLACK);
            title.setTextSize(18);
            title.setGravity(Gravity.CENTER_HORIZONTAL);
            holder.ll1.addView(title);

            // Initialize a new CardView
            CardView card = new CardView(mContext);
            // Set the CardView layoutParams
            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(
                    RecyclerView.LayoutParams.MATCH_PARENT,
                    RecyclerView.LayoutParams.MATCH_PARENT
            );
            card.setLayoutParams(params);
            // Set CardView corner radius
            card.setRadius(9);
            // Set cardView content padding
            card.setContentPadding(15, 15, 15, 15);
            // Set a background color for CardView
            card.setCardBackgroundColor(Color.parseColor("#FFC6D6C3"));
            // Set the CardView maximum elevation
            card.setMaxCardElevation(15);
            // Set CardView elevation
            card.setCardElevation(9);
            LinearLayout parent = new LinearLayout(getApplicationContext());

            parent.setLayoutParams(new LinearLayout.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            parent.setOrientation(LinearLayout.VERTICAL);
            for (Map.Entry<String, Integer> entry : albumList) {
//                holder.title.setText(entry.getKey() + "\t\t" + entry.getValue());
                if (entry.getValue() / 10 == pos) {

                    TextView tv = new TextView(getApplicationContext());
                    tv.setText(entry.getKey() + "\t\t" + entry.getValue());
                    tv.setPadding(10,4,4,4);
                    tv.setTextSize(15);
                    tv.setTextColor(Color.BLACK);

                    // Put the TextView in CardView
                    parent.addView(tv);

                }

                Log.e("MainActivity", pos+"  "+entry.getKey() + "\t\t" + entry.getValue() + " --- " + albumList.size()+" -- "+datacount.size());
            }
            card.addView(parent);
            holder.ll1.addView(card);

            View v1 = new View(getApplicationContext());
            v1.setMinimumHeight(2);
            v1.setBackgroundColor(Color.BLUE);
            holder.ll1.addView(v1);
        }

        @Override
        public int getItemCount() {
            return datacount.size();
        }
    }
}
